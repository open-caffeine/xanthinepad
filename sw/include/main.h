#include <stdexcept>
#include <string>

class RuntimeError: public std::runtime_error {
	using std::runtime_error::runtime_error;

public:
	template<typename... Args>
	static RuntimeError format(const char* format, Args... args)
	{
		std::string_view sv(format);
		const size_t len = 2 * sv.length();
		// i hope this length is sufficient
		char* c = new char[len + 1];
		snprintf(c, len, format, args...);
		auto e = RuntimeError(c);
		delete[] c;
		return e;
	}
};

class InvalidState: public std::exception {
	using std::exception::exception;
};

class InvalidInput: public RuntimeError {
	using RuntimeError::RuntimeError;
	using RuntimeError::format;
public:
	InvalidInput(): InvalidInput("fehlerhafte Eingabe") {}
};

class SlotError: public RuntimeError {
	using RuntimeError::RuntimeError;
	using RuntimeError::format;
};

