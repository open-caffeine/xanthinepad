#pragma once

#include <array>

class LEDs
{
public:
	enum Setting: uint8_t {
		OFF = 0,
		GREEN = 1,
		RED2 = 2,
		RED1 = 3,
		YELLOW = 4,
		R1G = 5,
		YR2 = 6,
		SETTING_NUM
	};

	LEDs(const std::array<uint8_t, 4>& pins);

	void set(Setting);

private:
	void setRaw(uint8_t);

	const std::array<uint8_t, 4> pins;
};

