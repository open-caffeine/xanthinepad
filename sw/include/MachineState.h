#pragma once

#include "MachineFlags.h"

#include <Product.h>

enum class HandshakeState
{
	UNINITIALIZED,
	WAITING,
	STARTED,
	DONE
};

class MachineState
{
public:
	MachineState():
		handshakeState(HandshakeState::UNINITIALIZED),
		anyProductMade(false)
	{}

	MachineFlags getFlags() const { return flags; }
	void setFlags(const MachineFlags& mf) { flags = mf; }

	HandshakeState getHandshakeState() const { return handshakeState; }
	void setHandshakeState(HandshakeState s) { handshakeState = s; }

	void setLastProduct(const Product& p) {
		lastProductMade = p;
		anyProductMade = true;
	}

	bool hasMadeProductYet() const { return anyProductMade; }

	Product getLastProduct() const {
		return lastProductMade;
	}

	void setBrewing(bool f) { brewing = f; }
	bool isBrewing() const { return brewing; }

	
private:
	MachineFlags flags;
	HandshakeState handshakeState;
	bool anyProductMade;
	Product lastProductMade;
	bool brewing;
};
