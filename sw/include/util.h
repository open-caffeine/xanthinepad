#pragma once

#include <array>
#include <driver/gpio.h>

namespace util {

template<typename T>
class array_view
{
public:
	template<std::size_t N>
	constexpr array_view(const std::array<T, N>& arr) noexcept:
		ptr(arr.data()), num(N)
	{}

	const T* begin() noexcept { return ptr; }
	const T* end() noexcept { return ptr + num; }
private:
	const T* const ptr;
	const std::size_t num;
};

void doPinConf(array_view<uint8_t> pins, gpio_mode_t mode, gpio_pullup_t pullup_en);

unsigned long millis();
}
