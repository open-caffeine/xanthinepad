#pragma once

class MachineState;
class Product;

bool startProduct(const Product& p);
void displayOutput(const char* txt);
void machine_task(MachineState* state);

