#ifndef LED_TASK_H
#define LED_TASK_H

#include "MessageStructs.h"
#include <mutex>

class MsgManagerImpl;
class JuraCoffeeMachine;

class MsgStateProvider {
public:
	MsgStateProvider(MsgManagerImpl& manager);
	~MsgStateProvider();

	void setLEDState(const LEDState&);
	void setAlert(Alert&&);
	void setString(const DisplayString&);

private:
	MsgManagerImpl& manager;
	Alert currentAlert;
	LEDState currentState;
	DisplayString currentString;

	friend class MsgManagerImpl;
};

class MsgManagerImpl {
public:
	MsgManagerImpl(const std::array<uint8_t, 4>& pins);

	void update();
	void attachProvider(MsgStateProvider&);
	void detachProvider(MsgStateProvider&);
	void attachMachine(JuraCoffeeMachine& cm) { machine = &cm; }

private:
	void recomputeLEDPriorities();
	void recomputeStrPriorities();
	void recomputeAlertPriorities();

	void doRecomputeLEDPriorities();
	void doRecomputeStrPriorities();
	void doRecomputeAlertPriorities();

	void putDisplayString(const std::string&);

	std::mutex mut;
	LEDs leds;
	Alert currentAlert;
	LEDState currentState;
	DisplayString currentString;
	std::vector<MsgStateProvider*> providers;
	bool hadAlertSet = false;
	bool displayTextInvalid = true;
	JuraCoffeeMachine* machine = nullptr;

	friend class MsgStateProvider;
};

class MsgManager {
public:
	static MsgManagerImpl& instance();
	static void update() { instance().update(); }
	static void attachMachine(JuraCoffeeMachine& cm) { instance().attachMachine(cm); }
	static MsgStateProvider createProvider() {
	   	return MsgStateProvider(instance());
	}
};

#endif
