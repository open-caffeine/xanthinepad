#pragma once

#include <Product.h>
#include <stdexcept>

class Persistence {
public:
	static Persistence& instance() {
		static Persistence inst;
		return inst;
	}

	class Entry {
	public:
		Product read();
		void write(const Product&);
	private:
		Entry(int num): num(num) {}
		int num;
		friend class Persistence;
	};

	void dump();

	Entry operator[](int);

	class Error: public std::runtime_error {
		using std::runtime_error::runtime_error;
	};

	class EmptySlot: public std::exception {
		using std::exception::exception;
	};

private:
	Persistence();
	~Persistence();
};
