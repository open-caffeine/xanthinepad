#pragma once

#include <cstddef>

class Serial
{
public:
	Serial(int rxPin, int txPin);
	~Serial();
	void begin(unsigned int baudrate);
	int read();
	int available();
	void write(const char* data, size_t num);
};
