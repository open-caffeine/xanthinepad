#ifndef LED_OUTPUT_H
#define LED_OUTPUT_H

#include "LEDs.h"
#include <vector>

class MsgStateProvider;
class MsgManagerImpl;

class LEDState {
public:
	constexpr LEDState(unsigned int priority,
			LEDs::Setting first,
			LEDs::Setting second,
			unsigned long blinkPeriod): 
		priority(priority), first(first), second(second), period(blinkPeriod) {}

	constexpr LEDState(): LEDState(0, LEDs::OFF, LEDs::OFF, 0) {}

	bool operator==(const LEDState& other) const
	{
		return (priority == other.priority)
			&& (first == other.first)
			&& (second == other.second)
			&& (period == other.period);
	}

	LEDs::Setting getCurrentLEDSetting() const;

	static const LEDState STARTUP;
	static const LEDState HANDSHAKE_WAITING;
	static const LEDState HANDSHAKE_RUNNING;
	static const LEDState READY;
	static const LEDState INPUT_ACTIVE;
	static const LEDState MACHINE_BLOCKED;
	static const LEDState MACHINE_BUSY;
	static const LEDState FATAL_ERROR;

private:
	unsigned int priority;
	LEDs::Setting first;
	LEDs::Setting second;
	unsigned long period;

	friend class LEDStateProvider;
	friend class MsgManagerImpl;
	friend class MsgStateProvider;
};

class Alert
{
public:
	Alert(unsigned int priority, LEDs::Setting first,
			LEDs::Setting second, unsigned long blinkPeriod,
			unsigned long duration, std::string_view displayText);

	Alert(): Alert(0, LEDs::OFF, LEDs::OFF, 200, 0, "") {}

	bool hasEnded() const;

	LEDs::Setting getCurrentLEDSetting() const;

	static Alert inputError(std::string_view sv) {
		return Alert(150, LEDs::RED2, LEDs::YELLOW, 200, 2000, sv);
	}

	static Alert invalidState() {
		return Alert(155, LEDs::RED2, LEDs::RED1, 200, 2000, "nicht moeglich");
	}

private:
	unsigned int priority;
	LEDs::Setting first;
	LEDs::Setting second;
	unsigned long period;
	unsigned long until;

	std::string displayText;

	friend class MsgManagerImpl;
	friend class MsgStateProvider;
};

class DisplayString
{
public:
	/* if the string is empty, we clear the display */
	DisplayString(unsigned int priority, std::string_view str):
		priority(priority), text(str)
	{}

	DisplayString(): DisplayString(0, "") {}

	template<typename... Args>
	static DisplayString format(unsigned int priority, const char* format, Args... args) {
		char buf[80];
		snprintf(buf, 79, format, args...);
		return DisplayString(priority, buf);
	}

private:
	unsigned int priority;
	std::string text;

	friend class MsgManagerImpl;
	friend class MsgStateProvider;
};

constexpr LEDState LEDState::STARTUP{1, LEDs::OFF, LEDs::OFF, 200};
constexpr LEDState LEDState::HANDSHAKE_WAITING{50, LEDs::YELLOW, LEDs::OFF, 200};
constexpr LEDState LEDState::HANDSHAKE_RUNNING{50, LEDs::YELLOW, LEDs::YELLOW, 200};
constexpr LEDState LEDState::READY{1, LEDs::GREEN, LEDs::GREEN, 200};
constexpr LEDState LEDState::INPUT_ACTIVE{10, LEDs::GREEN, LEDs::OFF, 500};
constexpr LEDState LEDState::MACHINE_BLOCKED{20, LEDs::RED2, LEDs::RED2, 200};
constexpr LEDState LEDState::MACHINE_BUSY{40, LEDs::GREEN, LEDs::YELLOW, 200};
constexpr LEDState LEDState::FATAL_ERROR{255, LEDs::RED1, LEDs::RED2, 200};

#endif
