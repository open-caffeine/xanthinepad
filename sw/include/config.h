#pragma once

#include <array>

namespace conf {
namespace serial {
	static constexpr const int rxPin = 18;
	static constexpr const int txPin = 17;
};
static constexpr std::array<uint8_t, 4> ledPins{6, 15, 7, 16};
#ifdef REVERSE_KEYPAD
// in my current testing setup, keypad is plugged in in reverse
static constexpr std::array<uint8_t, 4> keypadYPins{9, 10, 11, 12};
static constexpr std::array<uint8_t, 3> keypadXPins{13, 14, 21};
#else
static constexpr std::array<uint8_t, 4> keypadYPins{21, 14, 13, 12};
static constexpr std::array<uint8_t, 3> keypadXPins{11, 10, 9};
#endif
};
