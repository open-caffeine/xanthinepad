#pragma once

#include <cstdint>
#include <array>
#include <exception>

class Keypad {
private:
	enum Event {
		INTERRUPT_FIRED = 0x1,
	};
public:
	Keypad(const std::array<uint8_t, 3>& pinX, const std::array<uint8_t, 4>& pinY);


	uint16_t getBitmask() const { return pressedMask; }
	char charFromMask(uint16_t) const;

	char popChar(int timeoutMs);

	static const char CANCEL = 0x18;

private:
	static void task(Keypad*);
	static void isr(Keypad*);

	void installInterrupt();
	void enterIdle();
	void exitIdle();
	uint16_t scan();
	void update();

	const std::array<uint8_t, 3> pinX;
	const std::array<uint8_t, 4> pinY;

	uint16_t pressedMask;
	uint16_t activeMask;

	void* eventGroup;
	void* charQueue;

	unsigned long pushedSince;
	unsigned long lastChangeTime;

};
