#pragma once

#include <vector>
#include <cstdint>
#include <stdexcept>
#include <functional>

#include "MessageManager.h"

struct Modification
{
	uint8_t modificationNr;
	uint16_t value;
};

class InputProcessor
{
public:
	using ModList = std::vector<Modification>;

	using RepeatCallback = std::function<void()>;
	using SlotCallback = std::function<void(uint8_t, const ModList&)>;

	InputProcessor();
	~InputProcessor();
	void put(char c);

	void setRepeatCallback(RepeatCallback cb) { repeatCallback = cb; }
	void setRunCallback(SlotCallback cb) { runCallback = cb; }
	void setProgramCallback(SlotCallback cb) { progCallback = cb; }

private:
	void run();
	void runSeq();
	void runSpecial();
	void runDirect(char);
	void runProg();

	char fetch();
	uint8_t fetchDigit();

	uint8_t inputSlot();

	void* inputQueueHandle;
	void* taskHandle;

	RepeatCallback repeatCallback;
	SlotCallback runCallback;
	SlotCallback progCallback;

	MsgStateProvider stateProvider;

	friend void inputProcessorTask(InputProcessor*);
};

