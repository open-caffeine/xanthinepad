#include "unity.h"

#include "JuraMessageView.h"
#include "Progress.h"
#include "Product.h"

#include <cassert>
#include <esp_log.h>

void test_ProgressParsing() {
	const char* input = "@TV:3C0206060809FFFFFFFF22FFFF00";
	JuraMessageView jmv(input);
	try {
		auto ptr = parseProgressFromMessageView(jmv);
		TEST_ASSERT_EQUAL(Progress::Type::BREWING,
				ptr->getType());
		BrewingProgress& bp(*static_cast<BrewingProgress*>(&*ptr));
		Product pro = Product::from(bp);
		StringBuffer sb(1024);
		pro.formatHumanReadable(sb);
		ESP_LOGI("test", "%s", sb.get());
	} catch (const CantParseProgress& e) {
		ESP_LOGE("test", "%s", e.what());
		TEST_FAIL();
	}
}

void test_StringBuffer() {
	StringBuffer a(1024);
	a.appendf("%uml", 102738);
	TEST_ASSERT_EQUAL_STRING("102738ml", a.get());
}

int do_tests() {
	UNITY_BEGIN();
	RUN_TEST(test_StringBuffer);
	RUN_TEST(test_ProgressParsing);
	return UNITY_END();
}

extern "C" void app_main() {
	ESP_LOGI("test", "starting");
	do_tests();
	ESP_LOGI("test", "done");
}
