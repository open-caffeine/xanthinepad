#include "InputProcessor.h"

#include "main.h"

#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <freertos/queue.h>

#include <esp_log.h>

static const char* TAG = "InputProcessor";

void inputProcessorTask(InputProcessor* ip)
{
	while (true) {
		ESP_LOGI(TAG, "restart");
		ip->run();
	}
}

InputProcessor::InputProcessor():
	inputQueueHandle(xQueueCreate(5, sizeof(char))),
	stateProvider(MsgManager::createProvider())
{
	xTaskCreate((TaskFunction_t) inputProcessorTask,
			"input-processor", 4*1024, this, 3, &taskHandle);
}

InputProcessor::~InputProcessor()
{
	if (inputQueueHandle)
		vQueueDelete((QueueHandle_t) inputQueueHandle);
	if (taskHandle)
		vTaskDelete((TaskHandle_t) taskHandle);
}

void InputProcessor::put(char c)
{
	xQueueSendToBack((QueueHandle_t) inputQueueHandle, &c, 0);
}

char InputProcessor::fetch()
{
	char c;
	while (xQueueReceive((QueueHandle_t) inputQueueHandle,
				&c, 100) != pdTRUE);
	if (c == '#')
		throw InvalidInput("abgebrochen");
	return c;
}

static bool isdigit(char c)
{
	return ('0' <= c) && ('9' >= c);
}

static uint8_t toInt(char c)
{
	return c - '0';
}

uint8_t InputProcessor::fetchDigit()
{
	char c = fetch();
	if (!isdigit(c)) {
		throw InvalidInput("Ziffer erwartet");
	}
	return toInt(c);
}

void InputProcessor::run()
{
	try {
		runSeq();
	} catch(const RuntimeError& e) {
		stateProvider.setAlert(Alert::inputError(e.what()));
	} catch(const InvalidState&) {
		stateProvider.setAlert(Alert::invalidState());
	}
}

void InputProcessor::runSeq()
{
	// Start state
	stateProvider.setLEDState(LEDState::READY);
	stateProvider.setString({});
	char in = fetch();
	stateProvider.setLEDState(LEDState::INPUT_ACTIVE);
	if (in == '0') {
		runSpecial();
	} else if (in == '*') {
		runProg();
	} else if (isdigit(in)) {
		runDirect(in);
	} else {
		throw InvalidInput();
	}
}

void InputProcessor::runDirect(char start)
{
	ESP_LOGI(TAG, "direct: %c", start);
	stateProvider.setString(
			DisplayString::format(10, "start: %c_", start));
	uint8_t slot = toInt(start) * 10;

	slot += fetchDigit();

	ESP_LOGI(TAG, "-> run %hhu", slot);
	if (runCallback)
		runCallback(slot, {});
}

void InputProcessor::runSpecial()
{
	ESP_LOGI(TAG, "special");
	stateProvider.setString({10, "FN: _"});
	char c = fetch();
	if (c == '0') {
		ESP_LOGI(TAG, "-> repeat");
		if (repeatCallback)
			repeatCallback();
		return;
	}
	throw InvalidInput("unbekannte Funktion");
}

void InputProcessor::runProg()
{
	ESP_LOGI(TAG, "prog");
	stateProvider.setString({10, "speichern unter: _"});
	uint8_t digit = fetchDigit();
	stateProvider.setString(
			DisplayString::format(10, "speichern unter: %hhu_", digit));
	uint8_t slot = digit * 10;
	slot += fetchDigit();
	if ((slot < 10) || (slot > 89)) {
		throw SlotError("kein Slot");
	}
	ESP_LOGI(TAG, "-> prog %hhu", slot);
	stateProvider.setString(
			DisplayString::format(10, "speichern unter: %hhu", slot));
	if (progCallback)
		progCallback(slot, {});
}

