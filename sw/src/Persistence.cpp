#include "Persistence.h"

#include <StringBuffer.h>

#include <esp_log.h>
#include <esp_spiffs.h>

#include <fstream>
#include <string>

using namespace std;
using Entry = Persistence::Entry;

static const char* TAG = "persistence";
static const string BASEPATH("/data");
static const string DATABASE("/data/products.db");
static const unsigned int SPACING = 64;

Persistence::Persistence() {
	ESP_LOGI(TAG, "mounting");

	const esp_vfs_spiffs_conf_t conf {
		.base_path = BASEPATH.c_str(),
		.partition_label = nullptr,
		.max_files = 2,
		.format_if_mount_failed = true,
	};

	esp_err_t ok = esp_vfs_spiffs_register(&conf);
	switch (ok) {
		case ESP_ERR_NO_MEM: // if objects could not be allocated
			ESP_LOGE(TAG, "out of memory");
			break;
		case ESP_ERR_INVALID_STATE: // if already mounted or partition is encrypted
			ESP_LOGE(TAG, "invalid state");
			break;
		case ESP_ERR_NOT_FOUND: // if partition for SPIFFS was not found
			ESP_LOGE(TAG, "no partition found");
			break;
		case ESP_FAIL: // if mount or format fail
			ESP_LOGE(TAG, "mount failed");
			break;
		default:
			break;
	};
}

Persistence::~Persistence() {
	ESP_LOGI(TAG, "unmounting");
	esp_vfs_spiffs_unregister(nullptr);
}

Entry Persistence::operator[](int num) {
	return {num};
}

Product Entry::read() {
	std::ifstream is(DATABASE);
	if (!is.is_open()) {
		const char* msg = "unable to open database for reading";
		ESP_LOGE(TAG, "%s", msg);
		throw Persistence::Error(msg);
	}

	is.seekg(SPACING * num);
	Product out;
	is >> out;

	if (out.raw[0] == 0) {
		throw Persistence::EmptySlot();
	}

	return move(out);
}

void Entry::write(const Product& p) {
	/* need:
	 * out - obviously
	 * ate - skip to end so we can use tellp to get the file length
	 * in - to avoid truncating the file on write
	 */
	fstream os(DATABASE, ios_base::in | ios_base::out | ios_base::ate);

	if (!os.is_open()) {
		const char* msg = "unable to open database for writing";
		ESP_LOGE(TAG, "%s", msg);
		throw Persistence::Error(msg);
	}

	long long int len = os.tellp() + 1LL;
	if (len < SPACING*100) {
		const char* msg = "database file too small";
		ESP_LOGE(TAG, "%s (%lli)", msg, len);
		throw Persistence::Error(msg);
	}

	os.seekp(SPACING * num);
	os << p;
}

void Persistence::dump() {
	ESP_LOGI(TAG, "dump starts ============");
	for (int i = 1; i < 100; i++) {
		try {
			Product p = operator[](i).read();
			StringBuffer sb(48);
			p.format(sb);
			ESP_LOGI(TAG, "%02i :: %s", i, sb.get());
		} catch(const Persistence::EmptySlot&) {
		} catch(const std::exception& e) {
			ESP_LOGE(TAG, "%s", e.what());
		}
	}
	ESP_LOGI(TAG, "dump ends =============");
}


