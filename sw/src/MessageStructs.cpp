#include "MessageStructs.h"
#include "MessageManager.h"
#include "util.h"

using util::millis;

Alert::Alert(unsigned int priority, LEDs::Setting first,
		LEDs::Setting second, unsigned long blinkPeriod,
		unsigned long duration, std::string_view displayText):
	priority(priority), first(first), second(second),
	period(blinkPeriod), until(millis() + duration),
	displayText(displayText)
{}

bool Alert::hasEnded() const
{
	return millis() >= until;
}

LEDs::Setting Alert::getCurrentLEDSetting() const
{
	if ((period == 0) || (millis() % period) < (period / 2)) {
		return first;
	}
	return second;
}

LEDs::Setting LEDState::getCurrentLEDSetting() const
{
	if ((period == 0) || (millis() % period) < (period / 2)) {
		return first;
	}
	return second;
}

