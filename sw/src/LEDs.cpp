#include "LEDs.h"

#include "util.h"

#include <driver/gpio.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

using namespace std;

static const uint8_t settingMap[] {
	/*OFF*/ 0b0000,
	/*GREEN*/ 0b0101,
	/*RED2*/ 0b0110,
	/*RED1*/ 0b1001,
	/*YELLOW*/ 0b1010,
	/*R1G*/ 0b1101,
	/*YR2*/ 0b1110,
};

LEDs::LEDs(const array<uint8_t, 4>& pins):
	pins(pins)
{
	util::doPinConf(pins, GPIO_MODE_OUTPUT, GPIO_PULLUP_DISABLE);
	set(OFF);
}

void LEDs::set(Setting s)
{
	setRaw(settingMap[s]);
}

void LEDs::setRaw(uint8_t mask)
{
	for (size_t i = 0; i < 4; ++i) {
		const uint8_t pin = pins[i];
		const bool v = (mask & (1 << (3 - i)));
		gpio_set_level((gpio_num_t) pin, v);
	}
}

