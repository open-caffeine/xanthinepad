
#include <JuraSerial.h>
#include <JuraObfuscation.h>
#include <JuraInterface.h>
#include <JuraCoffeeMachine.h>
#include <Progress.h>
#include <Product.h>

#include "config.h"
#include "MachineState.h"
#include "Serial.h"
#include "MessageManager.h"
#include "main.h"

#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <esp_log.h>

static const char* TAG = "machine_task";

JuraCoffeeMachine* globalMachine = nullptr;

void debugInCB(const JuraMessageView& jmv) {
	ESP_LOGI(TAG, "<< %s", jmv.toString().get());
}

void debugOutCB(const char* s, bool) {
	ESP_LOGI(TAG, ">> %s", s);
}

bool startProduct(const Product& p) {
	if (!globalMachine) {
		ESP_LOGE(TAG, "no global machine");
		throw InvalidState();
	}

	if (!globalMachine->isHandshakeDone()) {
		ESP_LOGE(TAG, "handshake running");
		throw InvalidState();
	}

	if (globalMachine->isMachineBusy()) {
		ESP_LOGE(TAG, "machine busy");
		throw InvalidState();
	}

	if (globalMachine->isMachineBlocked()) {
		ESP_LOGE(TAG, "machine busy");
		throw InvalidState();
	}

	StringBuffer sb(256);
	p.formatHumanReadable(sb);
	ESP_LOGI(TAG, "starting production: %s", sb.get());

	globalMachine->startProduction(p);
	return true;
}

void machine_task(MachineState* state) {
	Serial machineSerial(conf::serial::rxPin, conf::serial::txPin);
	JuraSerial<Serial> juraSerial(machineSerial);
	JuraObfuscator obfuscator(Permutation::P1, Permutation::P2);
	JuraInterface juraInterface(juraSerial, obfuscator);
	JuraCoffeeMachine machine(juraInterface);
	machine.attachInDebug(debugInCB, true);
	machine.attachOutDebug(debugOutCB);

	machine.attachFlagsCallback([state](MachineFlags f){
		if (state->getFlags() != f) {
			state->setFlags(f);
			ESP_LOGI(TAG, "flags: %010llx", f.raw);
		}
	});

	machine.attachProgressCallback([state](const Progress& p) {
		StringBuffer sb(1024);
		p.format(sb);
		if (p.getType() == Progress::Type::BREWING) {
			const BrewingProgress& b = static_cast<const BrewingProgress&>(p);
			const auto p = Product::from(b);
			state->setLastProduct(p);
			state->setBrewing(true);
			sb.append(" => ");
			p.format(sb);
		} else if (p.getType() == Progress::Type::ENJOY) {
			state->setBrewing(false);
		}

		ESP_LOGI(TAG, "progress: %s", sb.get());
	});

	MsgManager::attachMachine(machine);
	auto stateProvider = MsgManager::createProvider();
	stateProvider.setLEDState(LEDState::HANDSHAKE_RUNNING);

	/* give machine a second to boot until handshake is possible.
	 * else, we will wait additional 5 seconds
	 */
	vTaskDelay(pdMS_TO_TICKS(1000));

	machine.startHandshake(false);

	globalMachine = &machine;

	while (true) {
		do {
			machine.update();
		} while (machineSerial.available());

		const bool hsDone = machine.isHandshakeDone();
		const bool hsRunning = machine.isHandshakeRunning();
		const bool hsWaiting = machine.isHandshakeWaiting();
		state->setHandshakeState(
				hsDone ? HandshakeState::DONE :
				(hsRunning ? HandshakeState::STARTED :
				HandshakeState::UNINITIALIZED));
		if (!hsDone) {
			stateProvider.setLEDState(hsWaiting ?
					LEDState::HANDSHAKE_WAITING :
					LEDState::HANDSHAKE_RUNNING);
		} else {
			auto flags = state->getFlags();
			stateProvider.setLEDState(flags.isBlocked() ?
					LEDState::MACHINE_BLOCKED : (
						machine.isMachineBusy() ?
						LEDState::MACHINE_BUSY :
						LEDState::READY));
		}
		vTaskDelay(pdMS_TO_TICKS(10));
	}
}

static_assert(pdMS_TO_TICKS(10) > 0, "10 ms do not correspond to at least one tick");

