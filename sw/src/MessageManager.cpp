#include "MessageManager.h"
#include "config.h"
#include "MessageStructs.h"
#include <JuraCoffeeMachine.h>

#include <algorithm>

#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <esp_log.h>

static const char* TAG = "MessageManager";

static void msg_man_task(MsgManagerImpl* man)
{
	while (true) {
		man->update();
		vTaskDelay(pdMS_TO_TICKS(100));
	}
}

MsgManagerImpl::MsgManagerImpl(const std::array<uint8_t, 4>& pins):
	leds(pins)
{
	providers.reserve(20);
	leds.set(LEDs::OFF);
	xTaskCreate((TaskFunction_t) msg_man_task, "msg-manager", 1024*4,
		this, 3, nullptr);
}

MsgManagerImpl& MsgManager::instance()
{
	static MsgManagerImpl l(conf::ledPins);
	return l;
}

void MsgStateProvider::setLEDState(const LEDState& ls)
{
	if (ls == currentState)
		return;

	const auto managerPrio = manager.currentState.priority;
	const auto newPrio = ls.priority;
	const auto oldPrio = currentState.priority;
	currentState = ls;
	ESP_LOGD(TAG, "%p sets state prio %u", this, newPrio);

	if (managerPrio <= newPrio) {
		/* new priority is highest over all */
		manager.currentState = ls;
		ESP_LOGD(TAG, "new is highest");
	} else if (oldPrio > newPrio && managerPrio <= oldPrio) {
		/* new priority is lower than old, so another provider could
		 * have higher priority than our new one
		 */
		ESP_LOGD(TAG, "need to recompute");
		manager.recomputeLEDPriorities();
	}
	manager.update();
}

void MsgStateProvider::setAlert(Alert&& la)
{
	currentAlert = la;
	ESP_LOGI(TAG, "%p sets alert prio %u", this, la.priority);
	manager.recomputeAlertPriorities();
	manager.update();
}

void MsgStateProvider::setString(const DisplayString& ds)
{
	const auto manPrio = manager.currentString.priority;
	const auto newPrio = ds.priority;
	const auto oldPrio = currentString.priority;
	currentString = ds;
	if (manPrio <= newPrio) {
		manager.currentString = ds;
		manager.displayTextInvalid = true;
	} else if (oldPrio > newPrio && manPrio <= oldPrio) {
		manager.recomputeStrPriorities();
	}
	manager.update();
}

void MsgManagerImpl::recomputeLEDPriorities()
{
	std::unique_lock lock(mut);
	doRecomputeLEDPriorities();
}

void MsgManagerImpl::recomputeStrPriorities()
{
	std::unique_lock lock(mut);
	doRecomputeStrPriorities();
}

void MsgManagerImpl::recomputeAlertPriorities()
{
	std::unique_lock lock(mut);
	doRecomputeAlertPriorities();
}

void MsgManagerImpl::doRecomputeLEDPriorities()
{
	LEDState* maxState = nullptr;
	for (auto p: providers) {
		if (!maxState)
			maxState = &p->currentState;

		if (maxState->priority <= p->currentState.priority) {
			maxState = &p->currentState;
		}
	}
	if (maxState)
		currentState = *maxState;
}

void MsgManagerImpl::doRecomputeStrPriorities()
{
	DisplayString* maxString = nullptr;
	for (auto p: providers) {
		if (!maxString) {
			maxString = &p->currentString;
		} else if (maxString->priority <= p->currentString.priority) {
			maxString = &p->currentString;
			displayTextInvalid = true;
		}
	}
	if (maxString)
		currentString = *maxString;
}

void MsgManagerImpl::doRecomputeAlertPriorities()
{
	Alert* maxAlert = nullptr;
	for (auto p: providers) {
		if (!maxAlert)
			maxAlert = &p->currentAlert;

		if (!p->currentAlert.hasEnded()
				&& (maxAlert->priority <= p->currentAlert.priority)) {
			maxAlert = &p->currentAlert;
		}
	}
	if (maxAlert)
		currentAlert = *maxAlert;
}

void MsgManagerImpl::putDisplayString(const std::string& sv)
{
	if (!machine)
		return;

	Display d = machine->getDisplay();

	if (sv.empty()) {
		d.clear();
	} else {
		d.output(sv.c_str());
	}
}

void MsgManagerImpl::update()
{
	std::unique_lock lock(mut);
	LEDs::Setting s = LEDs::OFF;
	if (!currentAlert.hasEnded()) {
		if (!hadAlertSet) {
			putDisplayString(currentAlert.displayText);
			hadAlertSet = true;
			ESP_LOGI(TAG, "alert starts");
		}
		s = currentAlert.getCurrentLEDSetting();
	} else if (hadAlertSet) {
		hadAlertSet = false;
		ESP_LOGI(TAG, "alert ends");
		putDisplayString(currentString.text);
	} else {
		s = currentState.getCurrentLEDSetting();
		if (displayTextInvalid) {
			displayTextInvalid = false;
			putDisplayString(currentString.text);
		}
	}
	leds.set(s);
}

void MsgManagerImpl::attachProvider(MsgStateProvider& sp)
{
	std::unique_lock lock(mut);
	ESP_LOGI(TAG, "attached provider %p", &sp);
	providers.push_back(&sp);
}

void MsgManagerImpl::detachProvider(MsgStateProvider& sp)
{
	std::unique_lock lock(mut);
	std::remove(providers.begin(), providers.end(), &sp);
	ESP_LOGI(TAG, "removed provider %p", &sp);
}

MsgStateProvider::MsgStateProvider(MsgManagerImpl& manager):
	manager(manager)
{
	manager.attachProvider(*this);
}

MsgStateProvider::~MsgStateProvider()
{
	manager.detachProvider(*this);
}


