#include "Serial.h"
#include <driver/uart.h>
#include <esp_log.h>

static const char* TAG = "Serial";

// use secondary UART for machine io, primary is debug
static constexpr uart_port_t theUART = UART_NUM_2;

Serial::Serial(int rxPin, int txPin)
{
	const uart_config_t conf {
		.baud_rate = 9600,
		.data_bits = UART_DATA_8_BITS,
		.parity = UART_PARITY_DISABLE,
		.stop_bits = UART_STOP_BITS_1,
		.flow_ctrl = UART_HW_FLOWCTRL_DISABLE,
		.rx_flow_ctrl_thresh = 0,
		.source_clk = UART_SCLK_APB,
	};
	ESP_ERROR_CHECK(uart_param_config(theUART, &conf));
	ESP_ERROR_CHECK(uart_set_pin(theUART, txPin, rxPin, -1, -1));
	ESP_ERROR_CHECK(uart_driver_install(theUART, 1024, 1024, 10, nullptr, 0));
}

Serial::~Serial()
{
	ESP_ERROR_CHECK(uart_driver_delete(theUART));
	ESP_LOGE(TAG, "destroyed");
}

void Serial::begin(unsigned int baudrate)
{
	ESP_ERROR_CHECK(uart_set_baudrate(theUART, baudrate));
	ESP_ERROR_CHECK(uart_flush_input(theUART));
}

int Serial::read()
{
	// dont know if uart_read_bytes adds a \0 at end
	char buf[2];
	const int ok = uart_read_bytes(theUART, buf, 1, 0);
	if ((ok < 0) || (ok > 1))
		ESP_LOGE(TAG, "uart_read_bytes returned %i", ok);
	if (ok == 1)
		return buf[0];
	return -1;
}

int Serial::available()
{
	size_t num;
	ESP_ERROR_CHECK(uart_get_buffered_data_len(theUART, &num));
	return num;
}

void Serial::write(const char* data, size_t n)
{
	const int out = uart_write_bytes(theUART, data, n);
	if (out != n)
		ESP_LOGE(TAG, "uart_write_bytes reports %i bytes written, %u requested", out, n);
	ESP_ERROR_CHECK(uart_wait_tx_done(theUART, pdMS_TO_TICKS(100)));
}


