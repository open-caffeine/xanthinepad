#include "util.h"
#include <esp_timer.h>

using namespace util;

unsigned long util::millis()
{
	return esp_timer_get_time() / 1000UL;
}

void util::doPinConf(array_view<uint8_t> pins, gpio_mode_t mode, gpio_pullup_t pullup_en)
{
	uint64_t bitmask = 0;
	for (auto p: pins) {
		bitmask |= (1 << p);
	}

	gpio_config_t conf {
		.pin_bit_mask = bitmask,
		.mode = mode,
		.pull_up_en = pullup_en,
		.pull_down_en = GPIO_PULLDOWN_DISABLE,
		.intr_type = GPIO_INTR_DISABLE,
	};
	ESP_ERROR_CHECK(gpio_config(&conf));

}
