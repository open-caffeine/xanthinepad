#include "config.h"

#include "main.h"

#include "Keypad.h"
#include "Serial.h"
#include "InputProcessor.h"
#include "MachineState.h"
#include "machine_task.h"
#include "MessageManager.h"
#include "Persistence.h"

#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <driver/gpio.h>
#include <esp_log.h>
#include <esp_system.h>

#include <cctype>

static const char* TAG = "main";

MachineState machineState;

class JuraSerialDebugCallback
{
	static constexpr size_t SIZE = 8;
	const char* tag;
	char buf_lit[SIZE + 1];
	char buf_cook[SIZE*3 + 1];
	size_t loc = 0;

public:
	JuraSerialDebugCallback(const char* tag): tag(tag) {};

	void operator()(char c) {
		sprintf(buf_cook + 3 * loc, "%02x ", c);
		buf_lit[loc++] = (isprint(c) ? c : '.');
		if (loc >= SIZE || c == '\n') {
			buf_lit[loc] = '\0';
			ESP_LOGI(tag, "%s| %s", buf_cook, buf_lit);
			loc = 0;
		}
	}
};

void repeatLastProduct()
{
	if (!machineState.hasMadeProductYet()) {
		ESP_LOGE(TAG, "no product made yet");
		throw InvalidState();
	}

	startProduct(machineState.getLastProduct());
}

void saveProduct(uint8_t slot, const InputProcessor::ModList& mods)
{
	if (!machineState.hasMadeProductYet()) {
		ESP_LOGE(TAG, "no product made yet");
		throw InvalidState();
	}

	// TODO something with the mods
	Persistence::instance()[slot].write(machineState.getLastProduct());

	char buf[80];
	snprintf(buf, sizeof(buf), "%hhu gespeichert", slot);
}

void runProduct(uint8_t slot, const InputProcessor::ModList& mods)
{
	// TODO something with the mods
	try {
		Product p = Persistence::instance()[slot].read();
		startProduct(p);
	} catch(const Persistence::EmptySlot&) {
		throw InvalidInput();
	}
}

void main_task(void*)
{
	Keypad kp(conf::keypadXPins, conf::keypadYPins);
	InputProcessor p;
	auto stateProvider = MsgManager::createProvider();
	stateProvider.setLEDState(LEDState::STARTUP);

	Persistence::instance().dump();

	p.setRepeatCallback(repeatLastProduct);
	p.setProgramCallback(saveProduct);
	p.setRunCallback(runProduct);

	stateProvider.setLEDState(LEDState::READY);

	while (true) {
		char c = kp.popChar(-1);
		if (c == Keypad::CANCEL) {
			ESP_LOGE(TAG, "user restart");
			esp_restart();
		}
		p.put(c);
	}
}

extern "C" void app_main()
{
	gpio_install_isr_service(0);
	xTaskCreate(main_task, "main", 1024*4, nullptr, 2, nullptr);
	xTaskCreate((TaskFunction_t) machine_task, "machine", 1024*4,
			&machineState, 2, nullptr);
}
