#include "Keypad.h"

#include "util.h"

#include <driver/gpio.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <freertos/event_groups.h>
#include <freertos/queue.h>
#include <esp_log.h>
#include <stdexcept>

static const char* TAG = "kp";

using namespace std;
using util::millis;

Keypad::Keypad(const array<uint8_t, 3>& pinX, const array<uint8_t, 4>& pinY):
	pinX(pinX), pinY(pinY), pressedMask(0), eventGroup(xEventGroupCreate()),
	charQueue(xQueueCreate(12, sizeof(char))), pushedSince(0), lastChangeTime(0)
{
	util::doPinConf(pinX, GPIO_MODE_INPUT, GPIO_PULLUP_ENABLE);
	util::doPinConf(pinY, GPIO_MODE_INPUT, GPIO_PULLUP_ENABLE);
	installInterrupt();
	enterIdle();
	xTaskCreate((TaskFunction_t) Keypad::task, "keypad", 1024*2, (void*) this, 5, nullptr);
}

void IRAM_ATTR Keypad::isr(Keypad* kp)
{
	BaseType_t x = pdFALSE;
	xEventGroupSetBitsFromISR(kp->eventGroup, INTERRUPT_FIRED, &x);
}

void Keypad::installInterrupt()
{
	for (auto p: pinY) {
		ESP_ERROR_CHECK(gpio_set_intr_type(
					(gpio_num_t) p, GPIO_INTR_ANYEDGE));
		ESP_ERROR_CHECK(gpio_isr_handler_add(
					(gpio_num_t) p, (gpio_isr_t) isr, this));
	}
}

void Keypad::enterIdle()
{
	for (auto p: pinX) {
		const gpio_num_t pX = (gpio_num_t) p;
		ESP_ERROR_CHECK(gpio_set_direction(pX, GPIO_MODE_OUTPUT));
		ESP_ERROR_CHECK(gpio_set_level(pX, false));
	}
	xEventGroupClearBits(eventGroup, INTERRUPT_FIRED);
	for (auto p: pinY) {
		ESP_ERROR_CHECK(gpio_intr_enable((gpio_num_t) p));
	}
}

void Keypad::exitIdle()
{
	for (auto p: pinY) {
		ESP_ERROR_CHECK(gpio_intr_disable((gpio_num_t) p));
	}
	for (auto p: pinX) {
		const gpio_num_t pX = (gpio_num_t) p;
		ESP_ERROR_CHECK(gpio_set_level(pX, true));
		ESP_ERROR_CHECK(gpio_set_direction(pX, GPIO_MODE_INPUT));
	}
}


uint16_t Keypad::scan()
{
	uint16_t newBitmask = 0;
	for (int x = 0; x < 3; x++) {
		const gpio_num_t pX = (gpio_num_t) pinX[x];
		ESP_ERROR_CHECK(gpio_set_direction(pX, GPIO_MODE_OUTPUT));
		ESP_ERROR_CHECK(gpio_set_level(pX, false));
		vTaskDelay(pdMS_TO_TICKS(10));
		for (int y = 0; y < 4; y++) {
			int val = gpio_get_level((gpio_num_t) pinY[y]);
			if (!val) {
				newBitmask |= (1 << (y * 4 + x));
			}
		}
		ESP_ERROR_CHECK(gpio_set_level(pX, true));
		ESP_ERROR_CHECK(gpio_set_direction(pX, GPIO_MODE_INPUT));
	}
	return newBitmask;
}

void Keypad::update()
{
	const unsigned long now = millis();
	const uint16_t newBitmask = scan();
	// we are using the interrupt so we do not need to check if the bitmask has
	// changed, it will always have changed

	// only accept if held at least 100ms
	if ((lastChangeTime + 100) > now) {
		ESP_LOGI(TAG, "key ev %04x", newBitmask);
		return;
	}

	if (activeMask && !newBitmask) {
		// case: release - accept char
		char c = charFromMask(activeMask);
		if ((c == '#') && ((now - lastChangeTime) > 4000)) {
			c = CANCEL;
		}
		ESP_LOGI(TAG, "key up %04x (%c)", activeMask, c);
		xQueueSend((QueueHandle_t) charQueue, &c, 0);
	} else if (newBitmask && !activeMask) {
		// case: push - store pushdown time
		char c = charFromMask(newBitmask);
		ESP_LOGI(TAG, "key dn %04x (%c)", newBitmask, c);
		pushedSince = lastChangeTime;
	}

	activeMask = newBitmask;
	lastChangeTime = millis();
}

char Keypad::charFromMask(uint16_t bitmask) const
{
	const array<char, 16> map{'1', '2', '3', 'A',
				'4', '5', '6', 'B',
				'7', '8', '9', 'C',
				'*', '0', '#', 'D'};
	for (int i = 0; i < 16; ++i) {
		if (bitmask & (1 << i)) {
			return map[i];
		}
	}
	return '\0';
}

char Keypad::popChar(int timeoutMs)
{
	char c;
	if (!xQueueReceive((QueueHandle_t) charQueue, &c,
				(timeoutMs >= 0) ? pdMS_TO_TICKS(timeoutMs) : -1))
		throw std::underflow_error("no more chars in queue");
	return c;
}

void Keypad::task(Keypad* kp)
{
	while (true) {
		xEventGroupWaitBits(kp->eventGroup, INTERRUPT_FIRED, pdTRUE, pdTRUE, -1);
		kp->exitIdle();
		kp->update();
		kp->enterIdle();
	}
}

