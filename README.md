# XanthinePad
Keypad for replacing the Jura BlueFrog bluetooth module.

## Software
Built with platformio, see `sw/` subdirectory.

## Hardware
Designed with HorizonEDA, see `board/` subdirectory
